-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 11 Janvier 2016 à 17:41
-- Version du serveur: 5.5.46-0ubuntu0.14.04.2
-- Version de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `unicorn`
--

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `author` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`nid`),
  KEY `author` (`author`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pagename` varchar(50) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pages`
--

INSERT INTO `pages` (`pid`, `pagename`, `order`, `link`, `icon`) VALUES
(1, 'Accueil', 0, 'index.php', 'fa-home'),
(2, 'Actualités', 1, '#', 'fa-newspaper-o'),
(3, 'Autre', 2, '#', 'fa-hand-spock-o');

-- --------------------------------------------------------

--
-- Structure de la table `ranks`
--

CREATE TABLE IF NOT EXISTS `ranks` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `rankname` varchar(50) NOT NULL,
  `isadmin` tinyint(1) NOT NULL DEFAULT '0',
  `ismoderator` tinyint(1) NOT NULL DEFAULT '0',
  `isuser` tinyint(1) NOT NULL DEFAULT '1',
  `isbanned` tinyint(1) NOT NULL DEFAULT '0',
  `candelete` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ranks`
--

INSERT INTO `ranks` (`rid`, `rankname`, `isadmin`, `ismoderator`, `isuser`, `isbanned`, `candelete`) VALUES
(1, 'User', 0, 0, 1, 0, 0),
(2, 'Banned', 0, 0, 0, 1, 0),
(3, 'Admin', 1, 1, 1, 0, 0),
(4, 'Moderator', 0, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(48) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `token` varchar(8) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `avatar` varchar(135) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`,`email`,`token`),
  KEY `rank` (`rank`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `author AS uid` FOREIGN KEY (`author`) REFERENCES `users` (`uid`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `rank AS rid` FOREIGN KEY (`rank`) REFERENCES `ranks` (`rid`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
