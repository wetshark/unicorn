<?php

class Datas
{
	private $pages = [];
	private $news = [];

	private function refreshData($data)
	{
		$pdo = $this->dbAcess();

		if ($data === 'pages') {
			$query = $pdo->prepare(
				'SELECT 
					pid,
					pagename,
					link,
					icon
				FROM pages
				ORDER BY `order`'
			);
		} elseif ($data === 'news') {
			$query = $pdo->prepare(
				'SELECT 
					nid,
					name,
					content,
					author,
					`date` 
				FROM news
				WHERE published = 1
				ORDER BY `date`'
			);
		}
		$query->execute();

		$datas = $query->fetchAll(PDO::FETCH_ASSOC);

		if ($data === 'pages') {
			foreach ($datas as $value) {
				$this->pages[$value['pid']] = [
					'pageName' => $value['pagename'],
					'link' => $value['link'],
					'icon' => $value['icon'],
				];
			}
		} elseif ($data === 'news') {
			foreach ($datas as $value) {
				$this->news[$value['nid']] = [
					'name' => $value['name'],
					'content' => $value['content'],
					'author' => $value['author'],
					'date' => $value['date'],
				];
			}
		}
	}

	// Access Database

	private function dbAcess()
	{
		return ConnectDb::getInstance(true);
	}

	// Getters

	public function getNews()
	{
		$this->refreshData('news');
		return $this->news;
	}

	public function getPages()
	{
		$this->refreshData('pages');
		return $this->pages;
	}
}