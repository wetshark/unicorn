<?php

if(!isset($_SESSION['user'])){
	$connect = new Connect;

	if(isset($_GET['action']) && $_GET['action'] === 'register'){
		if(isset($_POST['login']) && isset($_POST['mail']) 
		&& isset($_POST['passwd1']) && isset($_POST['passwd2'])){
			
			$connect->register($_POST['login'], $_POST['mail'], $_POST['passwd1'], $_POST['passwd2']);
			$states = $connect->getStates();
			$action = $states['action'];
		} else {
			$action = 'register';
		}
	} elseif(isset($_GET['action']) && $_GET['action'] === 'validate'){
		if(isset($_GET['token']) && !empty($_GET['token'])){

			$connect->validate($_GET['token']);
			$states = $connect->getStates();
			$action = $states['action'];
		} else {
			$action = 'validate';
		}
	} elseif(!isset($_GET['action']) || ($_GET['action'] !== 'register' && $_GET['action'] !== 'validate')){
		if(isset($_POST['login'])	&& isset($_POST['passwd'])){
			$connect->connection($_POST['login'],$_POST['passwd']);
			
			$states = $connect->getStates();
			$action = $states['action'];
		} else {
			$action = 'connect';
		}
	}
} else {
	header('Location: index.php');
}