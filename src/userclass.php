<?php

class User
{
	private $uid;
	private $username;
	private $email;
	private $userImage;
	private $isAdmin = '0';
	private $isModerator = '0';
	private $isUser = '1';
	private $isBanned = '0';
	private static $_instance = null;
	
	public static function newUser($uid, $username, $email, $isAdmin, $isModerator, $isUser, $isBanned, $userImage)
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new User($uid, $username, $email, $isAdmin, $isModerator, $isUser, $isBanned, $userImage);
		}

		return self::$_instance;
	}

	public static function refreshUser($uid, $username, $email, $isAdmin, $isModerator, $isUser, $isBanned, $userImage)
	{
		self::$_instance = new User($uid, $username, $email, $isAdmin, $isModerator, $isUser, $isBanned, $userImage);

		return self::$_instance;
	}

	private function __construct($uid, $username, $email, $isAdmin, $isModerator, $isUser, $isBanned, $userImage)
	{
		$this->uid = $uid;
		$this->username = $username;
		$this->email = $email;
		$this->isAdmin = $isAdmin;
		$this->isModerator = $isModerator;
		$this->isUser = $isUser;
		$this->isBanned = $isBanned;
		if (is_null($userImage)) {
			$this->userImage = '../uploads/avatar/default.jpg';
		} else {
			$this->userImage = '../uploads/avatar/' . $userImage;
		}
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getUserImage()
	{
		return $this->userImage;
	}

	public function getUserId()
	{
		return $this->uid;
	}

	public function getStatus()
	{
		return [
		'isAdmin' => $this->isAdmin,
		'isModerator' => $this->isModerator,
		'isUser' => $this->isUser,
		'isBanned' => $this->isBanned,
		];
	}
}