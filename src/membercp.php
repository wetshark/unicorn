<?php

if (isset($_SESSION['user'])) {
	$connect = new Connect;

	if (isset($_POST['mail']) && !empty($_POST['mail'])) {
		$connect->alter($_SESSION['user']->getUserId(), array('mail' => $_POST['mail']));
		$states = $connect->getStates();
		$action = $states['action'];

	} elseif (isset($_POST['oldPasswd']) && !empty($_POST['oldPasswd'])
		&& isset($_POST['passwd1']) && !empty($_POST['passwd1'])
		&& isset($_POST['passwd2']) && !empty($_POST['passwd2'])) {
		$connect->alter($_SESSION['user']->getUserId(), array('oldPasswd' => $_POST['oldPasswd'], 'passwd1' => $_POST['passwd1'], 'passwd2' => $_POST['passwd2']));
		$states = $connect->getStates();
		$action = $states['action'];

	} elseif (isset($_FILES['avatar']) && !empty($_FILES['avatar'])) {
		$connect->alter($_SESSION['user']->getUserId(), array('avatar' => $_FILES['avatar']));
		$states = $connect->getStates();
		$action = $states['action'];

	}	else {
		$action = '';
	}
} else {
	header('Location: index.php?p=connect');
}