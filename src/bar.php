<?php

$filesAccepted = [
'image/png',
'image/gif',
'image/jpeg',
];
if($_FILES['file']['size'] < 5*pow(10, 6) && in_array($_FILES['file']['type'], $filesAccepted)){
	$dirName = explode('/', $_FILES['file']['type'])[1];
	$content = scandir('../uploads/');
	if (!in_array($dirName, $content)) {
		mkdir('../uploads/' . $dirName);
	}

	$content = scandir('../uploads/' . $dirName);
	$fileName = hash('sha512', file_get_contents($_FILES['file']['tmp_name'])) . '.' . $dirName;
	if (!in_array($fileName, $content)) {
		move_uploaded_file($_FILES['file']['tmp_name'], '../uploads/' . $dirName . '/' . $fileName);
		echo 'DONE';
	} else {
		echo 'FILE ALREADY EXISTS';
	}
} else {
	echo 'ERROR';
}