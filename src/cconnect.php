<?php

class Connect 
{
	private $action;

	public function __construct()
	{
		$this->action = null;
	}

	// Register

	public function register($login, $mail, $passwd1, $passwd2)
	{
		$this->action = 'fregister';

		if(!empty($login) && !empty($mail) && !empty($passwd1) && !empty($passwd2)) {
			if(strcmp($passwd1,$passwd2) === 0 
			&& preg_match('`^[\w\d][\w\d\._\-]+[\w\d]@[\w\d][\w\d\._\-]+[\w\d]\.[\w]{2,4}$`', $mail)){
				if(strlen($passwd1) >= 6){
					$this->registerDb($login, $mail, $passwd1);
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private function registerDb($login, $mail, $passwd)
	{
		$pdo = $this->dbAcess();

		$query = $pdo->prepare(
			'SELECT
				username,
				email
			FROM users'
		);
		$query->execute();

		$users = $query->fetchAll(PDO::FETCH_ASSOC);

		if(!in_array($login, $users) && !in_array($mail, $users)){
			$salt = hash('md4', dechex(mt_rand()));
			$hashPasswd = hash('tiger192,4', $salt . $passwd);
			$token = hash('adler32', dechex(mt_rand()));

			$query = $pdo->prepare(
				'INSERT INTO 
					users(
						username,
						email,
						password,
						hash,
						token,
						rank
					)
				VALUES (
					:username,
					:email,
					:password,
					:hash,
					:token,
					1
					)'
			);

			$query->bindValue(':username', $login, PDO::PARAM_STR);
			$query->bindValue(':email', $mail, PDO::PARAM_STR);
			$query->bindValue(':password', $hashPasswd, PDO::PARAM_STR);
			$query->bindValue(':hash', $salt, PDO::PARAM_STR);
			$query->bindValue(':token', $token, PDO::PARAM_STR);
			$query->execute();

			$this->action = 'vregister';
		} else {
			return false;
		}
	}
	
	// Validate

	public function validate($token)
	{
		$this->action = 'fvalidate';

		if(!empty($token) && strlen($token) === 8){
			$this->validateDb($token);
		} else {
			return false;
		}
	}

	private function validateDb($token)
	{
		$pdo = $this->dbAcess();

		$query = $pdo->prepare('SELECT token FROM users WHERE users.token = :token');
		$query->bindValue(':token', $token, PDO::PARAM_STR);
		$query->execute();

		$dbToken = $query->fetchColumn();
		if($dbToken !== false && strcmp($token,$dbToken) === 0){
			$query = $pdo->prepare('UPDATE users SET token = NULL, active = 1 WHERE users.token = :token');
			$query->bindValue(':token', $token, PDO::PARAM_STR);
			$query->execute();

			$this->action = 'vvalidate';
		} else {
			return false;
		}
	}

	// Connection

	public function connection($login, $passwd)
	{
		$this->action = 'fconnect';

		if(!empty($login) && !empty($passwd)){
			$this->connectionDb($login, $passwd);
		} else {
			return false;
		}
	}

	private function connectionDb($login, $passwd)
	{
		$pdo = $this->dbAcess();

		$query = $pdo->prepare(
			'SELECT
				uid,
				username,
				email,
				password,
				hash,
				ranks.isadmin,
				ranks.ismoderator,
				ranks.isuser,
				ranks.isbanned,
				active,
				avatar
			FROM users
			INNER JOIN ranks ON users.rank = ranks.rid
			WHERE users.username = :username
			AND users.active = 1'
		);
		$query->bindValue(':username', $login, PDO::PARAM_STR);
		$query->execute();

		$user = $query->fetch(PDO::FETCH_ASSOC);
		if($user !== false && strcmp($login, $user['username']) === 0){
			$hashPasswd = hash('tiger192,4', $user['hash'] . $passwd);
			if(strcmp($user['password'], $hashPasswd) === 0){
				$_SESSION['user'] = User::newUser(
					$user['uid'],
					$user['username'], 
					$user['email'], 
					$user['isadmin'], 
					$user['ismoderator'], 
					$user['isuser'], 
					$user['isbanned'], 
					$user['avatar']
				);
				$this->action = 'vconnect';
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// Alter

	public function alter($uid, $args=[])
	{
		$this->action = 'falter';

		if (array_key_exists('mail', $args) && !empty($args['mail']) 
			&& preg_match('`^[\w\d][\w\d\._\-]+[\w\d]@[\w\d][\w\d\._\-]+[\w\d]\.[\w]{2,4}$`', $args['mail'])) {
			$this->alterMailDb($uid, $args['mail']);
		} elseif (array_key_exists('oldPasswd', $args) && !empty($args['oldPasswd']) 
			&& array_key_exists('passwd1', $args) && !empty($args['passwd1']) 
			&& array_key_exists('passwd2', $args) && !empty($args['passwd2'])) {
			if(strcmp($args['passwd1'], $args['passwd2']) === 0){
				$this->alterPasswdDb($uid, $args['oldPasswd'], $args['passwd1']);
			}
		} elseif (array_key_exists('avatar', $args) && !empty($args['avatar'])) {
			
			$filesAccepted = [
				'image/png',
				'image/gif',
				'image/jpeg',
			];
			
			if ($args['avatar']['size'] < 2*pow(10, 6) && in_array($args['avatar']['type'], $filesAccepted)) {
				$extension = explode('/', $args['avatar']['type'])[1];
				$content = scandir(BASE_DIR . '/uploads/avatar/');
				$fileName = hash('sha512', file_get_contents($args['avatar']['tmp_name'])) . '.' . $extension;
				if (!in_array($fileName, $content)) {
					$this->alterAvatarDb($uid, $args['avatar'], $fileName);
				} elseif (in_array($fileName, $content)) {
					$this->alterAvatarDb($uid, false, $fileName);
				}
			}
		}
	}

	private function alterMailDb($uid, $mail)
	{
		$pdo = $this->dbAcess();
		$query = $pdo->prepare(
			'SELECT 
				email
			FROM users
			WHERE users.uid = :uid'
		);
		$query->bindValue(':uid', $uid, PDO::PARAM_STR);
		$query->execute();

		$mails = $query->fetchAll(PDO::FETCH_ASSOC);
		if(!in_array($mail, $mails)){
			$query = $pdo->prepare(
				'UPDATE 
					users
				SET email = :email
				WHERE users.uid = :uid'
			);
			$query->bindValue(':uid', $uid, PDO::PARAM_STR);
			$query->bindValue(':email', $mail, PDO::PARAM_STR);
			$query->execute();

			$this->refreshUser($uid, $pdo);
			$this->action = 'valter';
		} else {
			return false;
		}
	}

	private function alterPasswdDb($uid, $oldPasswd, $passwd)
	{
		$pdo = $this->dbAcess();
		$query = $pdo->prepare(
			'SELECT
				password,
				hash
			FROM users
			WHERE users.uid = :uid'
		);
		$query->bindValue(':uid', $uid, PDO::PARAM_STR);
		$query->execute();

		$user = $query->fetch(PDO::FETCH_ASSOC);
		$hashPasswd = hash('tiger192,4', $user['hash'] . $oldPasswd);
		$hashNewPasswd = hash('tiger192,4', $user['hash'] . $passwd);
		if (strcmp($user['password'], $hashPasswd) === 0) {
			$query = $pdo->prepare(
				'UPDATE
					users
				SET password = :hashPasswd
				WHERE users.uid = :uid'
			);
			$query->bindValue(':uid', $uid, PDO::PARAM_STR);
			$query->bindValue(':hashPasswd', $hashNewPasswd, PDO::PARAM_STR);
			$query->execute();

			$this->refreshUser($uid, $pdo);
			$this->action = 'valter';
		} else {
			return false;
		}
	}

	private function alterAvatarDb($uid, $avatar, $fileName)
	{
		if ($avatar !== false) {
			move_uploaded_file($avatar['tmp_name'], BASE_DIR . '/uploads/avatar/' . $fileName);
		}

		$pdo = $this->dbAcess();
		$query = $pdo->prepare(
			'UPDATE 
				users
			SET avatar = :fileName
			WHERE users.uid = :uid'
		);
		$query->bindValue(':uid', $uid, PDO::PARAM_STR);
		$query->bindValue(':fileName', $fileName, PDO::PARAM_STR);
		$query->execute();

		$this->refreshUser($uid, $pdo);
		$this->action = 'valter';
	}

	private function refreshUser($uid, $pdo)
	{
		$query = $pdo->prepare(
			'SELECT
				uid,
				username,
				email,
				password,
				hash,
				ranks.isadmin,
				ranks.ismoderator,
				ranks.isuser,
				ranks.isbanned,
				active,
				avatar
			FROM users
			INNER JOIN ranks ON users.rank = ranks.rid
			WHERE users.uid = :uid'
		);
		$query->bindValue(':uid', $uid, PDO::PARAM_STR);
		$query->execute();

		$user = $query->fetch(PDO::FETCH_ASSOC);
		if ($user !== false) {
			$_SESSION['user'] = User::refreshUser(
				$user['uid'],
				$user['username'], 
				$user['email'], 
				$user['isadmin'], 
				$user['ismoderator'], 
				$user['isuser'], 
				$user['isbanned'], 
				$user['avatar']
			);
		} else {
			return false;
		}
	}

	// Access Database

	private function dbAcess()
	{
		return ConnectDb::getInstance(true);
	}

	// Getters

	public function getStates()
	{
		return [
			'action' => $this->action,
		];
	}
}