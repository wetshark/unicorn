<?php

abstract class Controller
{
	public static function index()
	{
		$style = 'index';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/index.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}

	public static function error()
	{
		$style = 'error';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/error.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}

	public static function search()
	{
		require BASE_DIR . '/src/search.php';
		
		$style = 'search';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/search.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}

	public static function connect()
	{
		require BASE_DIR . '/src/cconnect.php';
		require BASE_DIR . '/src/connect.php';
		
		$style = 'connect';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/connect.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}

	public static function logout()
	{
		require BASE_DIR . '/src/logout.php';

		$style = 'connect';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/connect.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}

	public static function membercp()
	{
		require BASE_DIR . '/src/cconnect.php';
		require BASE_DIR . '/src/membercp.php';

		$style = 'membercp';
		$datas = new Datas;
		$pages = $datas->getPages();
		require BASE_DIR . '/views/header.phtml';
		require BASE_DIR . '/views/membercp.phtml';
		require BASE_DIR . '/views/footer.phtml';
	}
}