<?php

class Foo
{

	private static $_instance = null;
	private static $instanceNb = 0;
	private static $maxInstance = 2;

	public static function constructor()
	{
		echo 'FOOOOOOO<br>';
		self::$instanceNb += 1;
		return new Foo;
	}

	public static function instance()
	{
		if (self::$instanceNb < self::$maxInstance) {
			return self::constructor();
		} elseif (self::$instanceNb >= self::$maxInstance) {
			echo 'Max instances !!<br>';
		}
	}

	public static function getInstanceNb()
	{
		return self::$instanceNb;
	}
}


$foo1 = Foo::instance();
$foo2 = Foo::instance();
$foo3 = Foo::instance();
$foo4 = Foo::instance();
$foo5 = Foo::instance();
$foo6 = Foo::instance();
$foo7 = Foo::instance();
$foo8 = Foo::instance();
$foo9 = Foo::instance();
$foo10 = Foo::instance();
$foo11 = Foo::instance();
$foo12 = Foo::instance();

echo Foo::getInstanceNb();
echo '<form action="bar.php" enctype="multipart/form-data" method="post">
<input type="file" name="file">
<input type="submit">
</form>';