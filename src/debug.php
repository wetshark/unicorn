<?php

if(isset($_GET['generate']) && $_GET['generate'] === 'true'){
	echo 'Building random data ...<br>' . PHP_EOL; 
	@ob_flush();flush();

	$data = '';
	for ($i = 0; $i < 64000; $i++)
		$data .= hash('md5', rand(), true);

	echo strlen($data) . ' bytes of random data built !<br>' . PHP_EOL . PHP_EOL . 'Testing hash algorithms ...<br>' . PHP_EOL; 
	@ob_flush();flush();

	$results = array();
	foreach (hash_algos() as $v) {
		echo $v . PHP_EOL; 
		@ob_flush();flush();
		$time = microtime(true);
		$hash = hash($v, $data, false);
		echo ' => ' . '(' . strlen($hash) . ') ' . $hash . '<br>';
		$time = microtime(true) - $time;
		$results[$time * 1000000][] = "$v => ";
	}

	ksort($results);

	echo PHP_EOL . PHP_EOL . '<br> Results: <br>' . PHP_EOL; 

	$i = 1;
	foreach ($results as $k => $v)
		foreach ($v as $k1 => $v1)
			echo '<br> ' . str_pad($i++ . '.', 4, ' ', STR_PAD_LEFT) . '  ' . str_pad($v1, 30, ' ') . ($k / 1000) . ' ms' . PHP_EOL;

	echo '<br><br><a href="debug.php">Retour</a>';
} else {
	echo '<form action=""><input type="hidden" name="generate" value="true"><input type="submit" value="generate"></form>';
}