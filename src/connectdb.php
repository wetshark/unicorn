<?php

abstract class ConnectDb
{
	// instance de la classe
	private static $_instance = null;

	// La méthode singleton
	public static function getInstance($debug=false) {

		if(is_null(self::$_instance)) {
			self::$_instance = self::instance($debug);
		}

		return self::$_instance;
	}

	private static function instance($debug=false)
	{
		$host = '127.0.0.1';
		$dbname = 'unicorn';
		$dbuser = 'root';
		$dbpass = 'toor';
		$errmode = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

		if($debug === false){
			self::$_instance = new PDO("mysql:host=$host;dbname=$dbname", $dbuser, $dbpass);
		} elseif($debug === true){
			self::$_instance = new PDO("mysql:host=$host;dbname=$dbname", $dbuser, $dbpass, $errmode);
		}

		self::$_instance->exec('SET NAMES UTF8');
		return self::$_instance;
	}
}