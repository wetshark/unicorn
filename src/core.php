<?php

$pages = [
	'connect',
	'logout',
	'membercp',
];

if (isset($_GET['p']) && in_array($_GET['p'], $pages)) {
	Controller::{$_GET['p']}();
} elseif (isset($_GET['s']) && !empty($_GET['s'])) {
	Controller::search();
} elseif (isset($_GET['p']) && !in_array($_GET['p'], $pages)) {
	Controller::error();
} else {
	Controller::index();
}