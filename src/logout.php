<?php

if(isset($_SESSION['user'])){
	$action = 'logout';
	$_SESSION['user'] = '';
	unset($_SESSION['user']);
} else {
	header('Location: index.php');
}