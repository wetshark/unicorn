document.addEventListener('DOMContentLoaded', function() {
	document.getElementById('uploadButton').addEventListener('change', function() {
		if (this.value.indexOf('C:\\fakepath\\') !== -1) {
			document.getElementById('uploadFile').value = this.value.substr(12);
		} else {
			document.getElementById('uploadFile').value = this.value;
		}
	});
});