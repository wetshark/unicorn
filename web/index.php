<?php
define('BASE_DIR', realpath(dirname(dirname(__file__))));

require BASE_DIR . '/src/userclass.php';
session_start();

require BASE_DIR . '/src/connectdb.php';
require BASE_DIR . '/src/datas.php';
require BASE_DIR . '/src/controller.php';
require BASE_DIR . '/src/core.php';